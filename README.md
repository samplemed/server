# Server

Blog API built with Django.

## Development

- [Git](https://git-scm.com/)
- [Python](https://python.org)
- [Poetry](https://python-poetry.org/)
- [MariaDB](https://mariadb.com)

Clone the repository:

```
git clone https://gitlab.com/samplemed/server.git
cd server
```

Install the dependencies:

```
poetry install
poetry shell
```

Run the migrations:

```
python manage.py migrate
```

Run the application:

```
python manage.py runserver
```

## API schema

`http://localhost:8000/schema`

## Implementation

- Resilience: a custom logging system and health checks should be implemented.
- Perfomance:
  - The database tables include indexes for quick access to the data.
  - The web app was built as a SPA.
- Security: the sensitive API endpoints are behind a JSON Web Token (JWT) based authentication system.
- Simultaneity: possible approaches include:
  - Implementation of distributed mirror servers.
  - A cache server.
