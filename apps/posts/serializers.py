from rest_framework import serializers

from .models import Comment, Commenter, Keyword, Post


class AnonPostSerializer(serializers.ModelSerializer):
    keywords = serializers.SlugRelatedField(
        many=True, read_only=True, slug_field="description"
    )

    author = serializers.SlugRelatedField(
        read_only=True, slug_field="username"
    )

    class Meta:
        model = Post
        exclude = ["draft"]


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"


class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        fields = "__all__"


class CommenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commenter
        exclude = ["deleted"]


class CommentSerializer(serializers.ModelSerializer):
    author = serializers.SlugRelatedField(read_only=True, slug_field="name")

    class Meta:
        model = Comment
        fields = "__all__"
