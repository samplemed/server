from django.db import models

from ..users.models import User


class Keyword(models.Model):
    description = models.CharField(max_length=255, unique=True)


class Commenter(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    ip = models.GenericIPAddressField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True)


class Post(models.Model):
    keywords = models.ManyToManyField(Keyword)
    title = models.CharField(max_length=300)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    draft = models.BooleanField(default=False)


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author = models.ForeignKey(Commenter, on_delete=models.CASCADE)
    parent = models.ForeignKey("Comment", null=True, on_delete=models.SET_NULL)
    author_ip = models.GenericIPAddressField()
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
