from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r"keywords", views.KeywordViewSet)
router.register(r"comments", views.CommentViewSet)
router.register(r"", views.PostViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path(r"commenters", views.CommenterViewSet.as_view()),
]
