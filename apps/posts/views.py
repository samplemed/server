from rest_framework import generics, mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from .models import Comment, Commenter, Keyword, Post
from .serializers import (
    AnonPostSerializer,
    CommenterSerializer,
    CommentSerializer,
    KeywordSerializer,
    PostSerializer,
)


def get_ip(request):
    x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")

    if x_forwarded_for:
        ip = x_forwarded_for.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")

    return ip


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    @action(detail=True)
    def comments(self, request, *args, **kwargs):
        comments = Comment.objects.filter(post_id=kwargs.get("pk")).order_by(
            "-created"
        )

        status_res = None

        if comments.count() == 0:
            status_res = status.HTTP_404_NOT_FOUND

        data = CommentSerializer(comments, many=True).data

        for row in data:
            row.pop("author_ip")

        return Response(data, status=status_res)

    def get_serializer_class(self):
        if self.request.user.is_authenticated:
            return PostSerializer
        return AnonPostSerializer

    def get_queryset(self):
        query = Post.objects

        # TODO: queryset should obey a variable type until the end.
        queryset = None

        if self.request.user.is_authenticated:
            queryset = query.all()

        else:
            queryset = query.filter(draft=False)

        queryset = queryset.order_by("-created")

        author = self.request.query_params.get("author")

        if author is not None:
            queryset = queryset.filter(author__username=author)

        keywords = self.request.query_params.get("keywords")

        if keywords is not None:
            queryset = queryset.filter(
                keywords__description__in=keywords.split(" ")
            )
        return queryset

    def create(self, request):
        post = Post.objects.create(
            author=request.user,
            title=request.data["title"],
            content=request.data["content"],
        )

        post.save()

        for keyword in request.data["keywords"]:
            try:
                new_keyword = Keyword(description=keyword)
                new_keyword.save()
                post.keywords.add(new_keyword)

            except Exception:
                pass

        return Response(PostSerializer(post).data)


class KeywordViewSet(viewsets.ModelViewSet):
    queryset = Keyword.objects.all()
    serializer_class = KeywordSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


# TODO Use GenericViewSet and mixins instead. This will make urls.py uniform.
class CommenterViewSet(generics.CreateAPIView):
    queryset = Commenter.objects.all()
    serializer_class = CommenterSerializer
    permission_classes = [AllowAny]

    def create(self, request):
        commenter = Commenter(
            ip=get_ip(request),
            name=request.data["name"],
            email=request.data["email"],
        )

        commenter.save()

        return Response(CommenterSerializer(commenter).data)


class CommentViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [AllowAny]

    def create(self, request):
        parent = None

        if request.data["parent"] is not None:
            parent = Comment.objects.get(pk=request.data["parent"])

        comment = Comment(
            author=Commenter.objects.get(pk=request.data["author"]),
            author_ip=get_ip(request),
            post=Post.objects.get(pk=request.data["post"]),
            parent=parent,
            content=request.data["content"],
        )

        comment.save()

        return Response(CommentSerializer(comment).data)
