from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r"verify_credentials", views.CredentialsViewSet)
router.register(r"", views.UserViewSet)

urlpatterns = [
    path("", include(router.urls)),
]

urlpatterns = router.urls
