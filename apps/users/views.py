from rest_framework import mixins, permissions, viewsets
from rest_framework.response import Response

from .models import User
from .serializers import UserSerializer


class IsUserOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.id == request.user.id


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    permission_classes = [IsUserOrReadOnly]


class CredentialsViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def list(self, request):
        user = request.user

        return Response(UserSerializer(user).data)
